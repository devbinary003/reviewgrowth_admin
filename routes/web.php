<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.dashboard');
})->middleware('auth');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::resource('/users', 'UsersController');
Route::get('/users/deleteimage/{id}', 'UsersController@delete_image');
Route::get('/users/changepassword/{id}', 'UsersController@changepassword');
Route::post('/users/changepassword_update/{id}', 'UsersController@changepassword_update');

Route::resource('/profile','ProfileController');
Route::get('/profile/deleteimage/{id}','ProfileController@delete_image');
Route::get('/profile/changepassword/{id}', 'ProfileController@changepassword');
Route::post('/profile/changepassword_update/{id}', 'ProfileController@changepassword_update');

Route::resource('/businesslocations','BusinesslocationsController');
