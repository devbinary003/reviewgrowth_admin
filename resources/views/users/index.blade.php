@extends('layouts.admin-default',['user' => $user])
@section('title', 'Reviewgrowth | Users')
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
           Business Owners
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Business owners</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Business owners list</h3>
                </div><!-- /.box-header -->
                @include('layouts.flash-message')
                <div class="box-body">
                  @if(count($users)>0)
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Sr.no</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Access</th>
                        <th>Phone</th>
                        <th>Locations</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $count ='1'; ?>
                      @foreach ($users as $user_info)
                      <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{$user_info->fullname}}</td>
                        <td>{{$user_info->email}}</td>
                        <td>{{$user_info->hdpwd}}</td>
                        <td> {{$user_info->phone}}</td>
                        <td>{{$user_info->number_of_locations}}</td>
                        <td class="action_btns">
                        <a class="btn btn-default" href="{{ url('/users/'.$user_info->id.'/edit')}}" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a class="btn btn-default" href="{{ url('/users/'.$user_info->id)}}" title="View profile" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a class="btn btn-default" href="{{ url('/users/changepassword/'.$user_info->id)}}" title="change password" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-key" aria-hidden="true"></i></a>

                      <a class="btn btn-default" style="border: none; padding: 0px;">
                        <form class="delete" action="{{ route('users.destroy', $user_info->id) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <button type="submit" class="btn btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </form>
                      </a>
                      
                        <!-- {!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user_info->id], 'onclick' => 'delConfirm()', 'id' => 'delFrm', 'style'=>'display:inline']) !!}
                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default', 'data-toggle' => 'confirmation']) !!}
                        {!! Form::close() !!} -->
                      </td>
                       
                      </tr>
                      @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Sr.no</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Access</th>
                        <th>Phone</th>
                        <th>Locations</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                   @else
                    <table id="example1" class="table table-bordered table-striped">
                    <tr>
                      <td colspan="6" style="text-align: center;">No business owner found</td>
                    </tr>
                    </table>
                  @endif
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

  <script type="text/javascript">
    $( document ).ready(function() {    
      $(".delete").on("submit", function(){
          return confirm("Are you sure want to Delete?");
      });
    });
  </script>

    <script type="text/javascript">
      /*function delConfirm(){
        if( confirm("Are you sure want to Delete?") ){
         document.getElementById("delFrm").submit();
        }else{
          return false;
        }
      }*/
      $(function () {
        $("#example1").DataTable();
       /*$('#example1').DataTable({
          "paging": true,
          "lengthChange": true,
          "searching": true,
          "ordering": true,
          "info": true,
          "autoWidth": true
        });*/
      });
    </script>
@endsection 
