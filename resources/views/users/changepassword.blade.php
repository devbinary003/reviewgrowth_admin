@extends('layouts.admin-default', ['user' => $user])
@section('title', 'Reviewgrowth | Admin Change Password')
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Change Password
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Change Password</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
               @if($user_info->profilepic!='')
               <img class="profile-user-img img-responsive img-circle" src="{{ url('/public') }}/uploads/userprofileimage/thumbs/{{$user_info->profilepic}}" alt="{{$user_info->profilepic}}">
                @else
                <img class="profile-user-img img-responsive img-circle" src="{{ url('/public') }}/dist/img/user2-160x160.jpg" alt="User profile picture">
                @endif

              <h3 class="profile-username text-center">{{$user_info->fullname}}</h3>

              <p class="text-muted text-center">{{$user_info->email}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Phone</b> <a class="pull-right">{{$user_info->phone}}</a>
                </li>
               
              </ul>
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-md-9">

          <div class="nav-tabs-custom">
           @include('layouts.flash-message')
            <div class="tab-content">
              <div class="active tab-pane" id="settings">
                
                <form name="changepassword" id="changepassword" method="POST" action="{{ url('/users/changepassword_update/'.$user_info->id) }}" enctype="multipart/form-data" class="form-horizontal">
                  {{ csrf_field() }}
                 <!--  @method('PUT') -->
                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Current Password</label>
                  <div class="col-sm-10">
                      <input type="password" class="form-control" name="currentpassword" id="currentpassword" placeholder="current password" value="" required>
                     
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="newpassword" class="col-sm-2 control-label">New Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password" value="" required>
                     
                    </div>
                  </div>
                 
                  <div class="form-group">
                    <label for="newpassword_confirmation" class="col-sm-2 control-label">New Password confirmation</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="newpassword_confirmation" id="newpassword_confirmation" placeholder="Confirm password" required>
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                     <!--  <button type="submit" class="btn btn-danger">Update</button> -->
                     <input type="submit" class="btn btn-info"  id="passBtn" value="Submit">
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>

<script>
 jQuery('#changepassword').validate({
         rules: {
            currentpassword: {  
                required: true
            },
            newpassword: {  
                required: true
            },
            newpassword_confirmation: {
              required: true,   
              equalTo: "#newpassword"
            }
         },
         messages: {
            currentpassword: {
                required: "This field is required."
            },
            newpassword: {
                required: "This field is required."
            },
            newpassword_confirmation: {
                required: "This field is required.",
                equalTo: "Confirm new password does not match with new password"
            }
         },
    });
</script>


@endsection
