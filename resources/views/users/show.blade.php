@extends('layouts.admin-default',['user' => $user])
@section('title', 'Reviewgrowth | Users')
@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
       <!--  <li><a href="#">Examples</a></li> -->
        <li class="active">User profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-4">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
               @if($user_info->profilepic!='')
               <img class="profile-user-img img-responsive img-circle" src="{{ url('/public') }}/uploads/userprofileimage/thumbs/{{$user_info->profilepic}}" alt="{{$user_info->profilepic}}">
                @else
                <img class="profile-user-img img-responsive img-circle" src="{{ url('/public') }}/dist/img/user2-160x160.jpg" alt="User profile picture">
                @endif

              <h3 class="profile-username text-center">{{$user_info->fullname}}</h3>

              <p class="text-muted text-center">{{$user_info->email}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Phone</b> <a class="pull-right">{{$user_info->phone}}</a>
                </li>

                <li class="list-group-item">
                  <b>Number of Business loctions</b> <a class="pull-right">{{$user_info->number_of_locations}}</a>
                </li>
                <li class="list-group-item">
                  <b>Added locations</b> <a class="pull-right"><?php if(count($businesslocations) > 0 ) { echo count($businesslocations); } ?> </a>
                </li>
                <li class="list-group-item">
                  <b>Pending locations</b> <a class="pull-right"><?php 
                  if($user_info->number_of_locations!='' ) {
                    if(count($businesslocations) > 0 ) {
                       echo $user_info->number_of_locations - count($businesslocations);
                    }
                  }  
                 ?></a>
                </li>

                <li class="list-group-item">
                  <b>Total Amount</b> <a class="pull-right"><?php if($user_info->total_sum!='') {  echo '$'.number_format((float)$user_info->total_sum, 2, '.', ''); }  ?></a>
                </li>

                <li class="list-group-item">
                  <b>Number of Additional location</b> <a class="pull-right">{{$user_info->item_qnty}}</a>
                </li>

                <li class="list-group-item">
                  <b>Stripe Customer Id</b> <a class="pull-right">{{$user_info->customer_id}}</a>
                </li>

                <li class="list-group-item">
                  <b>Stripe Subscription Id</b> <a class="pull-right">{{$user_info->subscription_id}}</a>
                </li>

                <li class="list-group-item">
                  <b>Plan Id</b> <a class="pull-right">{{$user_info->plan_id}}</a>
                </li>

                <li class="list-group-item">
                  <b>Subscription Status</b> <a class="pull-right"><?php if($user_info->subscription_status == 'active') { echo 'Active'; } ?></a>
                </li>

                <li class="list-group-item">
                  <b>Subscription start at</b> <a class="pull-right"><?php if($user_info->start_date!='') { echo date('d/m/Y', $user_info->start_date); }?></a>
                </li>

                <li class="list-group-item">
                  <b>Card last four digit</b> <a class="pull-right">{{$user_info->card_number}}</a>
                </li>

                <li class="list-group-item">
                  <b>Card exp. month</b> <a class="pull-right">{{$user_info->card_exp_month}}</a>
                </li>

                <li class="list-group-item">
                  <b>Card exp. year</b> <a class="pull-right">{{$user_info->exp_year}}</a>
                </li>

              </ul>

            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
         
        </div>
        <!-- /.col -->
        <div class="col-md-8">
          
          <div class="nav-tabs-custom">
           
            <div class="tab-content">
             <h2>Business Locations</h2>
              <div class="active tab-pane" id="settings">
                @if(count($businesslocations)>0)
                <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="">Sr.no</th>
                  <th class="">Name</th>
                  <th class="">Address</th>
                  <th class="">Phone</th>
                  <th class="">Review Link</th>
                  <!--  <th class="">Action</th> -->
                </tr>
                </thead>
                <tbody>
                    <?php $count ='1'; ?>
                    @foreach ($businesslocations as $businesslocation)
                    <tr class="tablesection">
                      <td class="">{{ $count++ }}</td>
                      <td class="">{{$businesslocation->business_name}}</td>
                      <td class="">{{$businesslocation->business_address}}</td>
                      <td class="">{{$businesslocation->phone}}</td>
                      <td class="">{{$businesslocation->business_review_link}}</td>
                      <!-- <td class="action_btns">
                        <a class="btn btn-default" href="{{ url('/businesslocations/'.$businesslocation->id.'/edit')}}" title="Edit" data-toggle="tooltip" data-placement="bottom"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                        <a class="btn btn-default" href="{{ route('businesslocations.show',$businesslocation->id) }}" title="Show" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <a class="btn btn-default" href="{{ url('/admin/paymentHistory/'.$businesslocation->user_id.'/'.$businesslocation->id) }}" title="Payment" data-toggle="tooltip" data-placement="bottom"><i class="fa fa-money" aria-hidden="true"></i></a>
                        {!! Form::open(['method' => 'DELETE','route' => ['businesslocations.destroy', $businesslocation->id], 'onclick' => 'delConfirm()', 'id' => 'delFrm', 'style'=>'display:inline']) !!}
                        {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', ['class' => 'btn btn-default', 'data-toggle' => 'confirmation']) !!}
                        {!! Form::close() !!}
                      </td> -->
                    </tr>
                    @endforeach
                   
                </tbody>
                <tfoot>
                <tr>
                  <th class="">Sr.no</th>
                  <th class="">Name</th>
                  <th class="">Address</th>
                  <th class="">Phone</th>
                  <th class="">Review Link</th>
                 <!--  <th class="">Action</th> -->
                </tr>
                </tfoot>
              </table>
               @else
               <table id="example1" class="table table-bordered table-striped">
                    <tr>
                      <td colspan="6" style="text-align: center;">No business location found</td>
                    </tr>
                </table>
               @endif   
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
   <!-- /.content-wrapper -->
  <script type="text/javascript">
  function delConfirm(){
    if( confirm("Are you sure want to Delete?") ){
     document.getElementById("delFrm").submit();
    }else{
      return false;
    }
  }
  $(function () {
    $('#example1').DataTable();
  })
</script>

@endsection
