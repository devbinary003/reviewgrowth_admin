@extends('layouts.lgrg-default')
@section('title', 'Reviewgrowth | Reset Password')
@section('content')
    <div class="login-box">
      <div class="login-logo">
        <a href=""><b>Admin</b>Reviewgrowth</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
        <p class="login-box-msg">Reset Password</p>             
        <form action="{{ route('password.email') }}" method="POST">
          @csrf
          <div class="form-group has-feedback">
             <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" placeholder="Email" value="{{ old('email') }}" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
          </div>
         
          <div class="row">
            <div class="col-xs-4">
            </div><!-- /.col -->
            <div class="col-xs-8">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Send Password Reset Link</button>
            </div><!-- /.col -->
          </div>
        </form>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection    
