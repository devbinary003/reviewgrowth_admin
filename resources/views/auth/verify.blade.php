@extends('layouts.lgrg-default')
@section('title', 'Reviewgrowth | Login')
@section('content')
 <div class="login-box">
      <div class="login-logo">
        <a href=""><b>Admin</b>Reviewgrowth</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Verify Your Email Address</p>
        @if (session('resent'))
            <div class="alert alert-success" role="alert">
                {{ __('A fresh verification link has been sent to your email address.') }}
            </div>
        @endif
          <div class="row">
            <div class="col-xs-12">
                {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
            </div><!-- /.col -->
          </div>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection
