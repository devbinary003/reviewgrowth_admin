 <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
               @if(Auth::user()->profilepic!='')
                <img src="{{ url('/public') }}/uploads/userprofileimage/thumbs/{{Auth::user()->profilepic}}" class="img-circle" alt="{{Auth::user()->profilepic}}">
               @else
               <img src="{{ url('/public') }}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              @endif
            </div>
            <div class="pull-left info">
              <p>{{ Auth::user()->fullname}}</p>
              <a href="JavaScript:void(0);"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form> -->
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

             <li><a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>

             <li><a href="{{ route('profile.edit',Auth::user()->id) }}"><i class="fa fa-edit"></i> <span>Edit Profile</span></a></li>

             <li><a href="{{ url('/profile/changepassword/'.Auth::user()->id) }}"><i class="fa fa-edit"></i> <span>Change Password</span></a></li>

            <!--  <li><a href="{{url('/users')}}"><i class="fa fa-users"></i> <span>Business owners</span></a></li> -->

             <li class="treeview">
                <a href="JavaScript:Void(0);">
                  <i class="fa fa-users"></i>
                  <span>Business owners</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="{{url('/users')}}"><i class="fa fa-circle-o"></i>Business owners</a></li>
                </ul>
            </li>

             <li><a href="#"><i class="fa fa-cogs"></i> <span>Settings</span></a></li>

            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Business Owners</span>
                <span class="label label-primary pull-right">4</span>
              </a>
              <ul class="treeview-menu">
                <li><a href=""><i class="fa fa-circle-o"></i> </a></li>
              </ul>
            </li> -->
            
            <!-- <li>
              <a href="#">
                <i class="fa fa-th"></i> <span>Widgets</span> <small class="label pull-right bg-green">new</small>
              </a>
            </li> -->
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>