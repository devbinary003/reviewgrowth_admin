@extends('layouts.admin-default')
@section('title', 'Reviewgrowth | Admin user edit')
@section('content')
<div class="content-wrapper" style="min-height: 960px;">
  <section class="content-header">
    <h1>Edit User</h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a>
      </li>
      <li class="active">Edit User</li>
    </ol>
  </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
               @if($user_info->profilepic!='')
               <img class="profile-user-img img-responsive img-circle" src="{{ url('/public') }}/uploads/userprofileimage/thumbs/{{$user_info->profilepic}}" alt="{{$user_info->profilepic}}">
                @else
                <img class="profile-user-img img-responsive img-circle" src="{{ url('/public') }}/dist/img/user2-160x160.jpg" alt="User profile picture">
                @endif

              <h3 class="profile-username text-center">{{$user_info->fullname}}</h3>

              <p class="text-muted text-center">{{$user_info->email}}</p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Phone</b> <a class="pull-right">{{$user_info->phone}}</a>
                </li>
                
              </ul>
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
        </div>
        <!-- /.col -->
        <div class="col-md-9">

          <div class="nav-tabs-custom">
           
            <div class="tab-content">
             
              <div class="active tab-pane" id="settings">
               @include('layouts.flash-message') 
                <form name="editform" id="editform" method="POST" action="{{ route('users.update',$user_info->id) }}" enctype="multipart/form-data" class="form-horizontal">

                  {{ csrf_field() }}

                  @method('PUT')

                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">First Name</label>

                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="firstname" id="firstname" placeholder="First name" value="{{$user_info->firstname}}" required>
                    </div>
                  </div>


                 <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Last Name</label>

                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Last name" value="{{$user_info->lastname}}" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="{{$user_info->email}}" disabled>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="phone" class="col-sm-2 control-label">Phone</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="{{$user_info->phone}}" maxlength="10" required>
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">Profile Image</label>

                    <div class="col-sm-10">
                       <a class="button hollow app-attachment-upload-pickfiles" id="profileImage" href="javascript:void(0)" style="position: relative; z-index: 1;">Add Image</a>

                       <input type="file" id="profilepic" name="profilepic" accept="image/*" style="display:none;">

                       <div id="upload_pimg_msg"></div> 
                       <div class="" id="PreviewProfileImage" style="padding-top: 10px;">
                               @if($user_info->profilepic!='') 

                                <a href="javascript:void(0)" class="delete_btn" id="{{ $user_info->profilepic }}" title="Delete">Delete</a>

                                    <img src="{{ url('public') }}/uploads/userprofileimage/{{ $user_info->profilepic }}" class="img-responsive" style="width: 100px; height: 100px;">
                                   @endif
                              </div> 
                    </div>
                  </div>


                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                     <!--  <button type="submit" class="btn btn-danger">Update</button> -->
                     <input type="submit" class="btn btn-danger"  id="upBtn" value="Update">
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
 
</div>

<!-- /.content-wrapper -->
<script type="text/javascript">
// validate form on keyup and submit
 jQuery(document).ready( function() {

  jQuery('#profileImage').on('click', function() {
    jQuery('#profilepic').click();
  });

  jQuery('#profilepic').on('change', function() {
          jQuery('#PreviewProfileImage').show();  
          jQuery('#nofile').empty(); 
          jQuery('#PreviewProfileImage').empty(); 
          jQuery("#upload_pimg_msg").empty(); 
          imagesPreview(this, '#PreviewProfileImage');
  }); 

  var imagesPreview = function(input, placeToInsertImagePreview) {

    if (input.files) {
        var filesAmount = input.files.length;
        if(filesAmount > 1) {

          jQuery("#upload_pimg_msg").html("<span class='msg-error'>Exceeding max. file upload limit.</span>");
          return false;

         } else {

           var fileSize = 0;
           var fileName = '';

           for (i = 0; i < filesAmount; i++) {

            fileSize = fileSize+input.files[i].size;

            if(fileSize > 2097152) {
                fileName = fileName+input.files[i].name;
                jQuery("#upload_pimg_msg").html("<span class='msg-error'>" + fileName + " Exceeding max. upload size limit.</span>");
                return false;

               } else {

              var reader = new FileReader();

              reader.onload = function(event) {
                jQuery($.parseHTML('<img style="margin:0 10px; width:100px; height:100px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
            }

            reader.readAsDataURL(input.files[i]);

            }

          }

        }
    }

  }; 

    jQuery(".delete_btn").on('click', function(e){ 
    if(confirm("Are you sure you want to delete")){
        var profilepic = jQuery(this).attr('id');
      if(profilepic!='') {
      jQuery.ajax({
            type : 'GET',
            url  : "{{ url('/users') }}/deleteimage/{{ $user_info->id }}",
            success :  function(resp) {
                 if(resp==1){
                   jQuery('#PreviewProfileImage').hide();   
                   location.reload(); 
                 }
               }
            });
           }
         } else {
         return false;
         }
    });
      
  jQuery("#editform").validate({  
    rules: {
        firstname: {
          required: true,
          maxlength: 190
        },
        lastname: {
          required: true,
          maxlength: 190
        },
        phone: {
          required: true,
          number: true,
          minlength: 10,
          maxlength: 12
        }
      },
    messages: {
        firstname: {
          required: "This is required field.", 
          maxlength: "Maximum 190 characters allowed."
        },
        lastname: {
          required: "This is required field.", 
          maxlength: "Maximum 190 characters allowed."
        },
        phone: {
          required: "This is required field.", 
          number: "Please enter a valid number.",  
          minlength: "Minimum 10 digits required.",
          maxlength: "Maximum 12 digits allowed."
        } 
    },
    
    submitHandler: function(form) {
            form.submit();
      }
    });   

 });
</script>

@endsection