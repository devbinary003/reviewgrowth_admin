@extends('layouts.admin-default')
@section('title', 'Reviewgrowth | Admin User Add')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
    Add User
    <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('/dashboard')}}"><i class="fa fa-dashboard"></i> Dashboard</a>
    </li>
    <li class="active">Add User</li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Add User</h3>
    </div>
    @include('layouts.flash-message')
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" id="addform" method="post" action="{{route('users.store')}}" autocomplete="off">
      {{ csrf_field() }}
      <div class="col-md-6"> 
        <div class="box-body">
          <div class="form-group">
            <label for="inputName" class="col-sm-3 control-label">First name</label>
            <div class="col-sm-9">
              <input type="text" name="firstname" class="form-control" id="firstname" placeholder="First Name" value ="">
            </div>
          </div>
          <div class="form-group">
            <label for="inputName" class="col-sm-3 control-label">Last name</label>
            <div class="col-sm-9">
              <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Last Name" value ="">
            </div>
          </div>
          <div class="form-group">
            <label for="inputEmail" class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
              <input type="text" name="email" class="form-control" id="email" placeholder="Enter email" value ="">
            </div>
          </div>
          
          <div class="form-group">
            <label for="inputphone" class="col-sm-3 control-label">Phone</label>
            <div class="col-sm-9">
              <input type="text" name="phone" class="form-control" id="phone" placeholder="phone" value ="">
            </div>
          </div>
          
          <div class="form-group">
            <label for="address" class="col-sm-3 control-label">Profile Image</label>
            <div class="col-sm-9">
              <a class="button hollow app-attachment-upload-pickfiles" id="profileImage" href="javascript:void(0)" style="position: relative; z-index: 1;">Add Image</a>
              <input type="file" id="profilepic" name="profilepic" accept="image/*" style="display:none;">
              <div id="upload_pimg_msg"></div>
              <div class="" id="PreviewProfileImage" style="padding-top: 10px;">
              </div>
            </div>
          </div>
           <div class="form-group">
            <label for="inputPassword" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-9">
              <input type="password" name="password" class="form-control" id="password" placeholder="Enter password" value ="">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword" class="col-sm-3 control-label">Confirm  Password</label>
            <div class="col-sm-9">
              <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Confirm  password" value ="">
            </div>
          </div>
          
        </div>
      </div>

      <!-- /.box-body -->
      <div class="row">
        <div class="col-sm-12">
          <div class="box-footer">
            <button type="submit" class="btn btn-info">Submit</button>
          </div>
        </div>
      </div>
      <!-- /.box-footer -->
      
    </form>
  </div>
  
  <!-- /.row -->
</section>
<!-- /.content -->
</div>

<!-- /.content-wrapper -->
<script type="text/javascript">
// validate form on keyup and submit
 jQuery(document).ready( function() {


  jQuery('#profileImage').on('click', function() {
    jQuery('#profilepic').click();
  });

  jQuery('#profilepic').on('change', function() {
          jQuery('#PreviewProfileImage').show();  
          jQuery('#nofile').empty(); 
          jQuery('#PreviewProfileImage').empty(); 
          jQuery("#upload_pimg_msg").empty(); 
          imagesPreview(this, '#PreviewProfileImage');
  }); 

  var imagesPreview = function(input, placeToInsertImagePreview) {

    if (input.files) {
        var filesAmount = input.files.length;
        if(filesAmount > 1) {

          jQuery("#upload_pimg_msg").html("<span class='msg-error'>Exceeding max. file upload limit.</span>");
          return false;

         } else {

           var fileSize = 0;
           var fileName = '';

           for (i = 0; i < filesAmount; i++) {

            fileSize = fileSize+input.files[i].size;

            if(fileSize > 2097152) {
                fileName = fileName+input.files[i].name;
                jQuery("#upload_pimg_msg").html("<span class='msg-error'>" + fileName + " Exceeding max. upload size limit.</span>");
                return false;

               } else {

              var reader = new FileReader();

              reader.onload = function(event) {
                jQuery($.parseHTML('<img style="margin:0 10px; width:100px; height:100px;">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
            }

            reader.readAsDataURL(input.files[i]);

            }

          }

        }
    }

  };     
  jQuery("#addform").validate({  
    rules: {
        firstname: {
          required: true,
          maxlength: 190
        },
        lastname: {
          required: true,
          maxlength: 190
        },
        email: {
            required: true,
            email: true
        },
        phone: {
          required: true,
          number: true,
          minlength: 10,
          maxlength: 12
        },
        password: {  
            required: true,
            minlength: 6
          },
        password_confirmation: { 
            required: true,
            equalTo: "#password"
        }
      },
    messages: {
        firstname: {
          required: "This is required field.", 
          maxlength: "Maximum 190 characters allowed."
        },
        lastname: {
          required: "This is required field.", 
          maxlength: "Maximum 190 characters allowed."
        },
        email: {
          required:"This is required field.",
          email: "Please enter valid email."
        },
        phone: {
          required: "This is required field.", 
          number: "Please enter a valid number.",  
          minlength: "Minimum 10 digits required.",
          maxlength: "Maximum 12 digits allowed."
        }, 
        password: {
          required: "This is required field.",
          minlength: "Maximum 6 characters allowed."
        },
        password_confirmation: {
           required: "This is required field.",
           equalTo: "password not matched."
        }     
    },
    
    submitHandler: function(form) {
            form.submit();
      }
    });   

 });
</script>
@endsection