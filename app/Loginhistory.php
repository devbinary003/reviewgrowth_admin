<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loginhistory extends Model
{

	protected $table = 'loginhistory';

    protected $fillable = [
        'user_id', 'login_time', 'logout_time', 'browser', 'operating_system', 'ip_address', 'status', 'deleted', 'created_at', 'updated_at',
    ];

    protected $hidden = [
    ];

    
}
