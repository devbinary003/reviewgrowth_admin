<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Businessreview;
use App\Businesslocations;
use App\Livereviews;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        try {
          $user_id = Auth::id();
          $user = User::where('id', $user_id)->first(); 

          $usersCount = User::where('status', '=', '1')->where('isdeleted', '=', '0')->count();

          $locationsCount = Businesslocations::where('status', '=', '1')->where('isdeleted', '=', '0')->count();

          $lvrvCount = Livereviews::where('status', '=', '1')->where('is_deleted', '=', '0')->count();

          $locrvCount = Businessreview::where('status', '=', '1')->where('is_deleted', '=', '0')->count();

          return view('pages.dashboard',compact('user', 'usersCount', 'locationsCount', 'lvrvCount', 'locrvCount'));
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }         
    }

    
}
