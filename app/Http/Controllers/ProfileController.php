<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Businessreview;
use App\Businesslocations;
use App\Livereviews;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class ProfileController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
       try{   

        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }  
       
    }

    public function create()
    {   
      try{

      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }   
    }

    public function store(Request $request)
    {   
      try{

      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }   
    }

    public function show($id)
    {   
      try{

      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }   
    }

    public function edit($id)
    {   
      try{

        $user = User::where('id', $id)->first();
        return view('profile.edit', compact('user')); 

      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }   
    }

    public function update(Request $request, $id)
    {   
      try{

        $Input =  [];
        $firstname = ucfirst(trim($request->firstname));
        $Input['firstname'] = $firstname;
        $lastname = ucfirst(trim($request->lastname));
        $Input['lastname'] = $lastname;
        $Input['fullname'] = $firstname.' '.$lastname;
        $Input['phone'] = trim($request->phone);

        $updated_by = Auth::id();
        $Input['updated_by'] = $updated_by;
        $updated_at = date('Y-m-d H:i:s');
        $Input['updated_at'] = $updated_at;

        $upQry = User::where('id', $id)
                ->update($Input);

        if($request->hasfile('profilepic')) {
            $image = $request->file('profilepic');
            $filename = time() .'_'. $id .'-'. $image->getClientOriginalName();
            $image->move(public_path('uploads/userprofileimage/'), $filename);
            $thumb = Image::make(public_path('uploads/userprofileimage/'.$filename))->resize(300,200)->save(public_path('uploads/userprofileimage/thumbs/'. $filename),60); 

            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profilepic' => $filename]);
        } 

        return redirect()->route('profile.edit', $id)
                    ->with('success','Profile data updated successfully');   

      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }   
    }

    public function destroy($id)
    {   
      try{

      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        }   
    }

    public static function delete_image($id)
    {    
      try { 
        $Upquery = User::where('id', $id)->update(['profilepic' => '']);
        if($Upquery) {
           echo '1';
         } else {
            echo '0';
          }
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }
    public function changepassword($id)
    {
      try { 
        $user_id = Auth::id();
        $user = User::where('id', $id)->first();
        return view('profile.change_password',compact('user')); 
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } 
    }

    public function changepassword_update(Request $request, $id)
    {
      try {
        $currentpassword = $request->currentpassword;
        $newpassword = $request->newpassword;
        $user = User::where('id',$id)->first(); 
        if (!Hash::check($request->currentpassword, $user->password)) {
          return redirect('/profile/changepassword/'.$id)
                    ->with('error','Current Password Does Not Matched.');
        } else {
          $password_update = User::where('id', $id)
                             ->update(['password' => Hash::make($newpassword), 'hdpwd' => $newpassword]);
          if($password_update!='') {
             return redirect('/profile/changepassword/'.$id)->with('success', 'Password Changed Successfully');          
          } else {
            return redirect('/profile/changepassword/'.$id)->with('error', 'Password Not Updated Successfully');
          }
        } 
      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
      } 
    }

    /*public function changepassword_update(Request $request,$id)
    {
      try {
        $currentpassword = $request->currentpassword;
        $newpassword = $request->newpassword;
        $user = User::where('id',$id)->first(); 
        if (!Hash::check($request->currentpassword, $user->password)) {
          return redirect('/profile/changepassword/'.$id)
                    ->with('error','Current password does not match.');
        } else {
          $password_update = User::where('id', $id)
                             ->update(['password' => Hash::make($newpassword),'hd_password'=>$newpassword]);
          if($password_update!='') {
             return redirect('/profile/changepassword/'.$id)->with('success', 'Password changed successfully');          
          }
          else {
            return redirect('/profile/changepassword/'.$id)->with('error', 'Password not updated successfully');
          }
          
        } 
      } 
        catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }*/
    
}
