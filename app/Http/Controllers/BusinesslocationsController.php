<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Businessreview;
use App\Businesslocations;
use App\Livereviews;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class Businesslocations extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        
          return view('businesslocations.index');
    }
    public function show($id)
    { 
      try { 
          return view('businesslocations.show');
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }
    public function create(Request $request)
    {
      try { 
        return view('businesslocations.create'); 
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } 
    }
     public function store(Request $request)
    {
      try {
        return redirect()->route('businesslocations.index')->with('success','Location Created Successfully'); 
      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
      } 
    } 
    public function edit($id)
    {
      try { 
        return view('businesslocations.edit'); 
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } 
    }
    public function update(Request $request, $id)
    {
      try {
        return redirect()->route('businesslocations.index', $id)
                    ->with('success','Location Updated Successfully');
      } 
        catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    } 
    public function destroy(Request $request, $id)
    {
      return redirect()->route('businesslocations.index')->with('success','User deleted successfully'); 
    }
    
}
