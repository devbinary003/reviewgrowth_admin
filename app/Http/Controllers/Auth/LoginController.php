<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Loginhistory;
use Carbon\Carbon; 

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated($request, $user)
    {  
            $logindata = [
                'user_id' => $user->id,
                'login_time' => Carbon::now(),
                'browser' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null,
                'operating_system' => isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null,
                'ip_address'  => isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null,
            ];

            $lhId = Loginhistory::create($logindata);
            User::where('id', $user->id)->update(['logins' => $user->logins + 1, 'last_login' => Carbon::now(), 'isonline' => '1', 'llhid' => $lhId->id]);  

        if($user->role==1) {
           return redirect('/dashboard');
        } else {
            Auth::logout();
                return redirect()->intended('/login')->with('error','Access denide for non admin users!');
        }    
    }
}
