<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Businessreview;
use App\Businesslocations;
use App\Livereviews;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Classes\ErrorsClass;
use Session;
use Config;
use DB;
use Image;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user_id = Auth::id();
        $user = User::where('id', $user_id)->first(); 
        $users =  User::where('role', '2')->where('status', '1')->where('isdeleted', '0')->orderBy('id','DESC')->get();
        // ->paginate(Config::get('constant.pagination'));
          return view('users.index',compact('user','users'));
    }
    public function show($id)
    { 
      try { 
         $user_id = Auth::id();
         $user = User::where('id', $user_id)->first();
         $user_info = User::where('id', $id)->first();
         $businesslocations =  Businesslocations::where('user_id', $id)->where('status', '1')->where('isdeleted', '0')->orderBy('id','DESC')->get();
          return view('users.show',compact('user_info','businesslocations','user'));
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }
    public function create(Request $request)
    {
      try { 
        $user_id = Auth::id();
        $user = User::where('id', $user_id)->first();
        return view('users.create',compact('user')); 
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } 
    }
     public function store(Request $request)
    {
      try {
        $Input =  [];
        $Input['email'] = $request->email;
        $firstname = ucfirst(trim($request->firstname));
        $Input['firstname'] = $firstname;
        $lastname = ucfirst(trim($request->lastname));
        $Input['lastname'] = $lastname;
        $Input['fullname'] = $firstname.' '.$lastname;
        $Input['password'] = Hash::make($request->password);
        $Input['hdpwd'] = $request->password;
        $Input['phone'] = $request->phone;
        $Input['role'] = '2';
        
        $Input['device'] = isset($_SERVER['SERVER_SOFTWARE']) ? $_SERVER['SERVER_SOFTWARE'] : null;
        $Input['browser'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null;
        $Input['ipaddress'] = isset($_SERVER['REMOTE_ADDR']) ? ip2long($_SERVER['REMOTE_ADDR']) : null;
        $Input['active_code'] = strtolower(Str::random(30));
        $created_by = Auth::id();
        $Input['created_by'] = $created_by;
        $updated_by = Auth::id();
        $Input['updated_by'] = $updated_by;
        $updated_at = date('Y-m-d H:i:s');
        $Input['updated_at'] = $updated_at;
        if($request->hasfile('profilepic')) {
            $image = $request->file('profilepic');
            $filename = time() .'_'. $image->getClientOriginalName();
            $image->move(public_path('uploads/userprofileimage/'), $filename);
            $thumb = Image::make(public_path('uploads/userprofileimage/'.$filename))->resize(300,200)->save(public_path('uploads/userprofileimage/thumbs/'. $filename),60); 
            $Input['profilepic'] = $filename;
        } 

        $email = $request->email;
        $checkexist  = User::where('email', $email)->first();
        if($checkexist){
          return redirect()->route('users.create')->with('error','Email Already Exists!');
        } else {
          $Qry = User::insert($Input);
          if($Qry){
             return redirect()->route('users.index')->with('success','User Created Successfully'); 
          } else{
              return redirect()->route('users.create')->with('error','Something Wrong!'); 
          }
        }
      } 
      catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
            return redirect('/404');
        } 
    } 
    public function edit($id)
    {
      try { 
        $user_id = Auth::id();
        $user = User::where('id', $user_id)->first();
        $user_info = User::where('id', $id)->first();
        return view('users.edit',compact('user_info','user')); 
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } 
    }
    public function update(Request $request, $id)
    {
      try {
        
        $Input =  [];
        $firstname = ucfirst(trim($request->firstname));
        $Input['firstname'] = $firstname;

        $lastname = ucfirst(trim($request->lastname));
        $Input['lastname'] = $lastname;

        $name = ucfirst(trim($request->name));
        $Input['fullname'] = $firstname.' '.$lastname;

        $phone = ucfirst(trim($request->phone));
        $Input['phone'] = $phone;

        $updated_by = Auth::id();
        $Input['updated_by'] = $updated_by;
        $updated_at = date('Y-m-d H:i:s');
        $Input['updated_at'] = $updated_at;

         if($request->hasfile('profilepic')) {
            $image = $request->file('profilepic');
            $filename = time() .'_'. $id .'-'. $image->getClientOriginalName();
            $image->move(public_path('uploads/userprofileimage/'), $filename);
            $thumb = Image::make(public_path('uploads/userprofileimage/'.$filename))->resize(300,200)->save(public_path('uploads/userprofileimage/thumbs/'. $filename),60); 

            $upImgQry = DB::table('users')
            ->where('id', $id)
            ->update(['profilepic' => $filename]);
        }

        $upQry = User::where('id', $id)
                ->update($Input);

        return redirect()->route('users.edit', $id)
                    ->with('success','User Data Updated Successfully');
      
      } 
        catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    } 
    public function destroy(Request $request, $id)
    { 
      /*$remove =  User::where('id', $id)
            ->update(['status' => '0','isdeleted' => '1']);*/

      $isUser =  DB::table('users')->where('id', $id)->where('role', '2')->where('status', '1')->where('isdeleted', '0')->count();

      if( $isUser > 0 ) {  

        $rmvUser = DB::table('users')->where('id', $id)->delete();  

        if($rmvUser){

          $isLocations =  DB::table('businesslocations')->where('user_id', $id)->where('status', '1')->where('isdeleted', '0')->count(); 
          if( $isLocations > 0 ) {
            $LocationIds =  DB::table('businesslocations')->where('user_id', $id)->where('status', '1')->where('isdeleted', '0')->pluck('id');
            $rmvLoc = DB::table('businesslocations')->whereIn('id', $LocationIds)->delete();
          }
          
          $isLvRv =  DB::table('live_reviews')->where('business_user_id', $id)->where('status', '1')->where('is_deleted', '0')->count(); 
          if( $isLvRv > 0 ) {
            $LvRvIds =  DB::table('live_reviews')->where('business_user_id', $id)->where('status', '1')->where('is_deleted', '0')->pluck('id');
            $rmvLvRv = DB::table('live_reviews')->whereIn('id', $LvRvIds)->delete();
          }
          
          $isLcRv =  DB::table('business_reviews')->where('business_user_id', $id)->where('status', '1')->where('is_deleted', '0')->count(); 
          if( $isLcRv > 0 ) {
            $LcRvIds =  DB::table('business_reviews')->where('business_user_id', $id)->where('status', '1')->where('is_deleted', '0')->pluck('id'); 
            $rmvLcRv = DB::table('business_reviews')->whereIn('id', $LcRvIds)->delete();
          }

          $isLh =  DB::table('loginhistory')->where('user_id', $id)->where('status', '1')->where('deleted', '0')->count();
          if( $isLh > 0 ) {
            $LhIds =  DB::table('loginhistory')->where('user_id', $id)->where('status', '1')->where('deleted', '0')->pluck('id');
            $rmvLh = DB::table('loginhistory')->whereIn('id', $LhIds)->delete();
          }
         
        return redirect()->route('users.index')->with('success','User deleted successfully'); 

        } else {
            return redirect()->route('users.index')->with('error','Sorry fail to delete');
        }
      } else {
        return redirect()->route('users.index')->with('error','Sorry fail to delete');
      }  
    }
    public static function delete_image($id)
    {    
      try { 
        $Upquery = User::where('id', $id)->update(['profilepic' => '']);
        if($Upquery) {
           echo '1';
         } else {
            echo '0';
          }
        } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
        } 
    }
    public function changepassword($id)
    {
      try { 
        $user_id = Auth::id();
        $user = User::where('id', $user_id)->first();
        $user_info = User::where('id', $id)->first();
        return view('users.changepassword',compact('user_info','user')); 
      } catch(\Illuminate\Database\QueryException $e) {
        $errorClass = new ErrorsClass();
        $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
        } 
    }
    public function changepassword_update(Request $request, $id)
    {
      try {
        $currentpassword = $request->currentpassword;
        $newpassword = $request->newpassword;
        $user = User::where('id',$id)->first(); 
        if (!Hash::check($request->currentpassword, $user->password)) {
          return redirect('/users/changepassword/'.$id)
                    ->with('error','Current Password Does Not Matched.');
        } else {
          $password_update = User::where('id', $id)
                             ->update(['password' => Hash::make($newpassword), 'hdpwd' => $newpassword]);
          if($password_update!='') {
             return redirect('/users/changepassword/'.$id)->with('success', 'Password Changed Successfully');          
          } else {
            return redirect('/users/changepassword/'.$id)->with('error', 'Password Not Updated Successfully');
          }
        } 
      } catch(\Illuminate\Database\QueryException $e) {
          $errorClass = new ErrorsClass();
          $errors = $errorClass->saveErrors($e);
      } catch(\Exception $e) {
            $errorClass = new ErrorsClass();
            $errors = $errorClass->saveErrors($e);
      } 
    }
}
